#!/usr/bin/python3
# vim:fileencoding=utf-8:sw=4:et
'''
 * Copyright (C) 2009-2016, Mozbugbox
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street - Fifth Floor, Boston, MA 02110-1301,
 * USA.

Two Classes:
    PDFDoc: The document model
    MuView: The view of the document.
'''
from __future__ import print_function, unicode_literals, absolute_import, division

CACHE_SIZE_2 = 2
CACHE_SIZE_8 = 8
CACHE_SIZE_32 = 32
PAGE_CACHE_SIZE = CACHE_SIZE_8

import sys
import os
import io
import logging as log
import functools
import json
import time
import enum
import math

try:
    functools.lru_cache
except:
    import lru_cache

# fitz has to be imported before Gtk, Gdk. Else segfault happens
import fitz
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import GObject, GLib, Gio, Gtk, Gdk, GdkPixbuf
from gi.repository import cairo as gcairo
import cairo

from . import util

EPSILON = sys.float_info.epsilon
NATIVE = sys.getfilesystemencoding()

class PDFError(Exception):
    pass

class Zoom(enum.IntEnum):
    to_width = -1
    to_page = -2
    origin = 1

# monkey patch to add __hash__ to Matrix, Rect
@util.monkeypatch_method(fitz.Matrix, "__hash__")
def matrix__hash__(self):
    """Hash Matrix to a tuple of matrix parameters, so we
    can use fitz.Matrix as hash key in lrc_cache"""
    return hash((self.a, self.b, self.c, self.d, self.e, self.f))

@util.monkeypatch_method(fitz.Rect, "__hash__")
def rect__hash__(self):
    return hash((self.x0, self.y0, self.x1, self.y1))

Identity = fitz.Matrix(fitz.Identity)

@functools.lru_cache(CACHE_SIZE_32)
def _zoom_matrix(zoom):
    matrix = fitz.Matrix(1, 1)
    matrix.prescale(zoom, zoom)
    return matrix
def zoom_matrix(zoom):
    """Cache friendly matrix factory"""
    if abs(zoom - 1) < EPSILON:
        matrix = Identity
    else:
        matrix = _zoom_matrix(zoom)
    return matrix

# round to floor
ZOOM_DIGITS = 6
ZOOM_PADDING = round(0.1**(ZOOM_DIGITS + 1) * 5, ZOOM_DIGITS + 1)
@functools.lru_cache(CACHE_SIZE_32)
def zoom_matrix_with_size(zoom, width, height, new_width, new_height):
    """Get matrix for the give zoom and size changes"""
    # deal with zoom < 0
    if zoom < 0:
        zoom = int(zoom)

    if new_width <= 0 or new_height <= 0:
        zoom = 1
    elif zoom == Zoom.to_width:  # fit width
        zoom = new_width / width
        zoom = round(zoom - ZOOM_PADDING, ZOOM_DIGITS)
    elif zoom == Zoom.to_page:  # fit page
        if new_width / width < new_height / height:
            zoom = new_width / width
        else:
            zoom = new_height / height
        zoom = round(zoom - ZOOM_PADDING, ZOOM_DIGITS)

    matrix = zoom_matrix(zoom)
    return matrix

@functools.lru_cache(CACHE_SIZE_32)
def matrix_transfer(matrix, x, y):
    """transform vector with matrix"""
    #import traceback;traceback.print_stack()
    if matrix != Identity:
        m = matrix
        x = x * m.a + y * m.c + m.e
        y = x * m.b + y * m.d + m.f
    return x, y

@functools.lru_cache(CACHE_SIZE_32)
def matrix_pre_translate(matrix, tx, ty, clone=True):
    """pre translate a matrix, return a new fitz.Matrix"""
    mat = fitz.Matrix(matrix) if clone else matrix
    mat.preTranslate(tx, ty)
    return mat

@functools.lru_cache(CACHE_SIZE_32)
def matrix_pre_rotate(matrix, degree, clone=True):
    """pre rotate a matrix, return a new fitz.Matrix"""
    mat = fitz.Matrix(matrix) if clone else matrix
    mat.prerotate(degree)
    return mat

def matrix_invert(matrix, clone=True):
    """Invert a matrix, return a new fitz.Matrix"""
    mat = fitz.Matrix(matrix) if clone else matrix
    mat.invert(mat)
    return mat

def rect_equal(r1, r2):
    """test 2 fitz.Rect for equality """
    return (r1.x0 == r2.x0 and r1.y0 == r2.y0 and
            r1.x1 == r2.x1 and r1.y1 == r2.y1)

def is_empty_rect(r):
    """test if rect is empty"""
    return r.x0 == r.x1 or r.y0 == r.y1

@functools.lru_cache(CACHE_SIZE_32)
def rect_transform(matrix, *rect):
    """transform rect, return a new rect"""
    rect = rect[0] if len(rect) == 1 else fitz.Rect(*rect)
    if rect.x0 > rect.x1 or rect.y0 > rect.y1:
        rect = rect_infinite_transform(matrix, rect)
    else:
        rect = fitz.Rect(rect.x0, rect.y0, rect.x1, rect.y1)
    return rect.transform(matrix)

def rect_infinite_transform(matrix, *rect):
    """Transfer a rect which might be infinite"""
    rect = rect[0] if len(rect) == 1 else fitz.Rect(*rect)
    rect_names = ["x0", "y0", "x1", "y1"]
    x0, y0, x1, y1 = [getattr(rect, x) for x in rect_names]
    xmin, xmax = min(x0, x1), max(x0, x1)
    ymin, ymax = min(y0, y1), max(y0, y1)

    rect_res = fitz.Rect(xmin, ymin, xmax, ymax)
    rect_res = rect_transform(matrix, rect_res)

    # preserve infinite rect after transfer
    if x0 > x1:
        t = rect_res.x1
        rect_res.x1 = rect_res.x0
        rect_res.x0 = t
    if y0 > y1:
        t = rect_res.y1
        rect_res.y1 = rect_res.y0
        rect_res.y0 = t
    return rect_res

def pixmap2pixbuf(rgba_data, width, height):
    """convert RGBA data into pixbuf"""
    if not isinstance(rgba_data, bytearray):
        rgba_data = bytearray(rgba_data)
    del rgba_data[3::4]  # strip alpha channel

    header = b"P6 %d %d 255\n" % (width, height)

    ploader = GdkPixbuf.PixbufLoader.new_with_type("pnm")
    ploader.write(header)
    ploader.write(rgba_data)
    ploader.close()
    pixbuf = ploader.get_pixbuf()
    return pixbuf

@functools.lru_cache(CACHE_SIZE_32)
def rect_gdk2fitz(r):
    """Convert Gdk.Rectangle to fitz.Rect"""
    rect = fitz.Rect(r.x, r.y, r.x + r.width, r.y + r.height)
    return rect

@functools.lru_cache(CACHE_SIZE_32)
def rect_fitz2gdk(r):
    """Convert fitz.Rect to Gdk.Rectangle """
    rect = Gdk.Rectangle()
    rect.x = r.x0
    rect.y = r.y0
    rect.width = r.width
    rect.height = r.height
    return rect

@functools.lru_cache(CACHE_SIZE_32)
def gdk_rect_new(x, y, width, height):
    """Cached Gdk.Rectangle"""
    rect = Gdk.Rectangle()
    rect.x = x
    rect.y = y
    rect.width = width
    rect.height = height
    return rect

class GData(GObject.GObject):
    """A GObject holding python data"""
    def __init__(self, data):
        GObject.GObject.__init__(self)
        self.data = data

    def _str_(self):
        return "GData<{}>".format(str(self.data))

class PDFDoc(GObject.GObject):
    '''A pdf document model for the pdf view.
    Attributes:
        page_count: Total pages in the document.
    '''
    def __init__(self, path):
        GObject.GObject.__init__(self)
        self.doc = None
        self.path = path
        self.page_cache_size = PAGE_CACHE_SIZE

        self.doc = fitz.Document(path)

    @property
    def page_count(self):
        pc = self.doc.page_count
        return pc

    @property
    def metadata(self):
        return self.doc.metadata

    @property
    def table_of_content(self):
        return self.doc.get_toc(simple=False)

    @property
    def outlines(self):
        return self.doc.outlines

    @functools.lru_cache(maxsize=PAGE_CACHE_SIZE)
    def get_link(self, page_num):
        """return a dict of {rect: fitz.linkDest}"""
        page = self.get_page(page_num)
        if page is None:
            return
        link = page.load_links()
        res = []
        while link is not None:
            # link.dest, link.rect are not properly refcounted
            dest = util.clone_object(link.dest)
            rect = util.clone_object(link.rect)
            res.append((rect, dest))
            link = link.next
        return res

    @functools.lru_cache(maxsize=PAGE_CACHE_SIZE)
    def get_page(self, page_num):
        '''Return a mupdf.Page for the given page number.
        '''
        if self.doc is None:
            raise PDFError("No PDF document")
        if not (0 <= page_num < self.page_count):
            raise PDFError("page_num too big: {}".format(page_num))
        ret = self.doc.load_page(page_num)
        return ret

    @functools.lru_cache(maxsize=CACHE_SIZE_2)
    def render(self, page_num, matrix=Identity, colorspace="RGB"):
        """Return a pixbuf """
        page = self.get_page(page_num)
        if not page:
            return
        pixmap = page.get_pixmap(matrix=matrix, colorspace=colorspace, alpha=True)

        data = pixmap.samples
        #start = time.time()
        # convert bytearray to bytes for GByte() speedup
        # see: https://bugzilla.gnome.org/show_bug.cgi?id=721497
        pixels = GLib.Bytes(bytes(data))
        pixbuf = GdkPixbuf.Pixbuf.new_from_bytes(pixels,
                GdkPixbuf.Colorspace.RGB, True, 8,
                pixmap.width, pixmap.height,
                pixmap.stride)
        # FIXME: Pixbuf.new_from_bytes() will leak GBytes so how. Work around
        # by make a copy of pixbuf.
        # https://gitlab.gnome.org/GNOME/pygobject/-/issues/127
        pixbuf = pixbuf.copy()
        #end = time.time()
        #print(end - start)
        #print(len(data), type(data))
        #pixbuf.savev("pixbuf.png", "png", [], [])

        return pixbuf

    def close(self):
        self.doc.close()
        del self.doc
        self.doc = None

    def page_icon(self, page_num, icon_size=128):
        """return a pixbuf"""
        if self.doc is None:
            return
        page = self.get_page(page_num)
        bound = page.bound()
        zoom = min(icon_size / bound.width, icon_size / bound.height)
        log.debug("front_page_icon zoom: {}".format(zoom))
        zoom = max(0.1, zoom)
        pix = self.render(page_num, zoom_matrix(zoom))
        #pix.savev("name.png", "png", [], [])
        return pix

class TextSelection:
    """PDF Text Selection Handler"""
    def __init__(self, hit_max=1024):
        self.hit_max = hit_max

        self.page = None
        self._text_tree = None

    def set_page(self, page):
        """Set current page"""
        self.page = page
        self._text_tree = None

    @property
    def text_tree(self):
        """ generate a stext_json like tree but with info for chars
        from page stext_xml. fitz/stext-output.c/fz_print_stext_page_as_xml()
        """
        if self._text_tree is not None:
            return self._text_tree

        if not self.page:
            return None

        tree = self.page.get_text(output="rawdict")
        group_names = ["blocks", "lines", "spans", "chars"]
        fRect = fitz.Rect

        def bbox2rect(adict):
            """Convert value of bbox from list of float to fRect"""
            for k in adict.keys():
                if k == "bbox":
                    adict[k] = fRect(*adict[k])
                elif k in group_names:
                    for elm in adict[k]:
                        bbox2rect(elm)
        bbox2rect(tree)

        self._text_tree = tree
        return tree

    def _unpack_rect(self, rect):
        """
        unpack rect to tupple such that (x0, y0) is always the upper point
        """
        x0, y0, x1, y1 = rect.x0, rect.y0, rect.x1, rect.y1
        if rect.y0 > rect.y1:
            x1, y1, x0, y0 = x0, y0, x1, y1
        return x0, y0, x1, y1

    def hilight_selection(self, rect):
        """return list of text region that was selected by rect"""
        # see: MuPDF fz_highlight_selection()
        if not self.page:
            return None

        hit_max = self.hit_max
        x0, y0, x1, y1 = self._unpack_rect(rect)
        xmin, xmax = min(x0, x1), max(x0, x1)

        hit_bboxes = []
        for block in self.text_tree["blocks"]:
            rect_block = block["bbox"]
            # exclude disjoint blocks
            rect_tmp = fitz.Rect(xmin, y0, xmax, y1)
            rect_tmp.intersect(rect_block)
            if is_empty_rect(rect_tmp):
                continue
            for line in block["lines"]:
                rect_line = line["bbox"]
                linebox = fitz.Rect()
                # whole line between y0, y1
                if rect_line.y0 > y0 and rect_line.y1 < y1:
                    linebox.includeRect(line["bbox"])
                    if len(hit_bboxes) < hit_max:
                        hit_bboxes.append(linebox)
                    continue
                for span in line["spans"]:
                    for pchar in span["chars"]:
                        charbox = pchar["bbox"]
                        cx0, cy0, cx1, cy1 = (charbox.x0, charbox.y0,
                                charbox.x1, charbox.y1)
                        hitting = False
                        # single line selection
                        if (cy0 <= y0 <= cy1 and cy0 <= y1 <= cy1):
                            if cx0 <= xmax and cx1 >= xmin:
                                hitting = True
                        # tail of top line and head of bottom line
                        elif ((cy0 <= y0 <= cy1 and cx1 >= x0) or
                                (cy0 <= y1 <= cy1 and cx0 <= x1)):
                            hitting = True
                        if hitting:
                            linebox.includeRect(charbox)

                if not is_empty_rect(linebox) and len(hit_bboxes) < hit_max:
                    hit_bboxes.append(linebox)

        return hit_bboxes

    def copy_selection(self, rect):
        """return text that was selected by rect"""
        if not self.page:
            return None
        x0, y0, x1, y1 = self._unpack_rect(rect)
        xmin, xmax = min(x0, x1), max(x0, x1)

        result = []
        for block in self.text_tree["blocks"]:
            rect_block = block["bbox"]
            # exclude disjoint blocks
            rect_tmp = fitz.Rect(xmin, y0, xmax, y1)
            rect_tmp.intersect(rect_block)
            if is_empty_rect(rect_tmp):
                continue
            for line in block["lines"]:
                line_buf = []
                rect_line = line["bbox"]
                # whole line between y0, y1
                if rect_line.y0 > y0 and rect_line.y1 < y1:
                    line_buf = [pchar["c"] for span in line["spans"]
                            for pchar in span["chars"]]
                    line_buf.append("\n")
                    result.append("".join(line_buf))
                    continue
                for span in line["spans"]:
                    for pchar in span["chars"]:
                        charbox = pchar["bbox"]
                        cx0, cy0, cx1, cy1 = (charbox.x0, charbox.y0,
                                charbox.x1, charbox.y1)
                        hitting = False
                        # single line selection
                        if (cy0 <= y0 <= cy1 and cy0 <= y1 <= cy1):
                            if cx0 <= xmax and cx1 >= xmin:
                                hitting = True
                        # tail of top line and head of bottom line
                        elif ((cy0 <= y0 <= cy1 and cx1 >= x0) or
                                (cy0 <= y1 <= cy1 and cx0 <= x1)):
                            hitting = True
                        if hitting:
                            line_buf.append(pchar["c"])

                if len(line_buf) > 0:
                    line_buf.append("\n")
                    result.append("".join(line_buf))

        res = "".join(result)
        return res

def check_cairo_operator(cr):
    """Draw squres to show effect of different cairo operator"""
    w = 100
    h = 100
    margin_x = 500
    margin_y = 500
    column = 5
    max_op = 29
    margin_text = 12
    bg = (.5, .5, .5)
    text_color = (1, 0, 0)
    cr.save()
    for i in range(max_op):
        if i in [10, 8, 4, 3]:
            continue
        cr.set_source_rgb(*bg)
        op = i
        x = (i % column) * w + margin_x
        y = (i // column) * h + margin_y
        cr.set_operator(op)
        cr.rectangle(x, y, w, h)
        cr.fill()
        cr.set_operator(cairo.OPERATOR_SOURCE)
        cr.move_to(x + margin_text, y + margin_text)
        cr.set_source_rgb(*text_color)
        cr.text_path(str(i))
        cr.fill()
    cr.restore()

class Clippy:
    """Clipboard handler"""
    def __init__(self):
        atom = Gdk.Atom.intern("CLIPBOARD", False)
        self.clip_clipboard = Gtk.Clipboard.get(selection=atom)

        atom = Gdk.Atom.intern("PRIMARY", False)
        self.clip_primary = Gtk.Clipboard.get(selection=atom)

    @property
    def primary_text(self):
        return self.clip_primary.wait_for_text()

    @primary_text.setter
    def primary_text(self, text):
        self.clip_primary.set_text(text, -1)

    @property
    def clipboard_text(self):
        return self.clip_clipboard.wait_for_text()

    @clipboard_text.setter
    def clipboard_text(self, text):
        self.clip_clipboard.set_text(text, -1)

class DraggingHandler:
    """Handle mouse dragging"""
    def __init__(self, muview):
        self.muview = muview
        self.moved = False
        self.dragging = False
        self.start = None
        self.end = None
        self.dragging_id = 0
        self.last_select_rects = None

    def reset(self):
        self.start = self.end = None
        self.dragging = self.moved = False
        self.last_select_rects = None
        if self.dragging_id > 0:
            GLib.source_remove(self.dragging_id)
            self.dragging_id = 0

    def handle_dragging(self):
        """dragging handler in timeout_add"""
        if self.dragging:
            ret = True
        else:
            ret = False  # Stop handle_dragging timeout
            self.dragging_id = 0
            self.muview.selected_to_clipboard("primary")

        if self.moved or self.start is None:
            self.moved = False
            if self.last_select_rects:
                margin = 5
                rect_all = fitz.Rect()
                [rect_all.includeRect(x) for x in self.last_select_rects]
                self.muview.queue_redraw_area(rect_all.x0, rect_all.y0,
                        rect_all.width, rect_all.height, margin)
            else:
                self.muview.queue_redraw()
        return ret

    def do_button_press(self, evt):
        timeout = 40  # handle dragging every timeout sec
        ret = False
        if evt.button == 1:
            self.dragging = True
            self.start = (evt.x, evt.y)
            if self.dragging_id <= 0:
                self.dragging_id = GLib.timeout_add(timeout,
                        self.handle_dragging)
            ret = True
        return ret

    def do_button_released(self, evt):
        ret = False
        if evt.button == 1 and self.dragging:
            self.dragging = False
            self.handle_dragging()
            if self.start == (evt.x, evt.y):
                self.start = None
                self.end = None
                self.last_select_rects = None
            else:
                self.end = (evt.x, evt.y)
                ret = True
        return ret

    def do_motion_notify(self, evt):
        ret = False
        if self.dragging:
            self.moved = True
            self.end = (evt.x, evt.y)
            ret = True
        return ret

class MuView(Gtk.ScrolledWindow):
    '''A MuPDF View that is a subclass of a scrolled Window.
        current_index: Load the given page on assignment. -1 == invalid.
    '''
    __gsignals__ = {
            'page-changed': (GObject.SIGNAL_RUN_FIRST, None, (int, int)),
            'page-rendered': (GObject.SIGNAL_RUN_FIRST, None,
                (gcairo.Context,)),
            }
    apply_background = GObject.Property(type=bool, default=True)
    background = GObject.Property(type=Gdk.RGBA,
            default=Gdk.RGBA(.5, .5, .5))
    canvas_color = GObject.Property(type=Gdk.RGBA,
            default=Gdk.RGBA(1.0, 1.0, 1.0, 1.0))
    invert_color = GObject.Property(type=bool, default=False)
    invert_background = GObject.Property(type=bool, default=False)
    # 12% of extra page to show
    column_extra_width = GObject.Property(type=float, default=.12)
    crop_rect = GObject.Property(type=Gdk.Rectangle)
    indicate_last_read = GObject.Property(type=bool, default=True)
    indicate_last_read_draw_line = GObject.Property(type=bool, default=False)
    read_indicator_width = GObject.Property(type=int, default=10)
    num_columns = GObject.Property(type=int, default=1)
    zoom = GObject.Property(type=float, default=1)
    rotate = GObject.Property(type=int, default=0)

    persistent_keys = ["apply_background", "background", "canvas_color",
            "crop_rect",
            "indicate_last_read", "indicate_last_read_draw_line",
            "num_columns", "read_indicator_width",
            "zoom", "rotate", "invert_color", "invert_background",
            ]
    persistent_doc_keys = ["current_index"]

    def __init__(self):
        Gtk.ScrolledWindow.__init__(self)

        self.handle_keyevent = True
        self.skip_draw_background = False

        self._current_index = -1
        self.zoom_step = 0.05
        self.zoom_step_more = 0.25
        self.link_color = (0.2, 0.5, 0.85)
        self.link_width = 2
        self.hint_links = False
        self.highlight_color = (0.7, 0.75, 0.85)
        self.jump_ratio = 0.2  # Find result centralization value.
        self.width = 1
        self.height = 1
        self.last_read_value = 0
        self.current_column = 0
        self.last_read_color = (1, .2, .0, 0.6)
        self._redraw_stop = False
        self.crop_rect = None

        self._cursor_pointer = None
        self._cursor_origin = None
        self.document = None
        self.dwg = None
        self.crop_dialog = None

        self.find_result = None  # a list of rectangle.
        self.find_index = 0

        self.preload_id = 0
        self.pre_render_id = 0

        self.setup_widget()
        self.dragger = DraggingHandler(self)
        self.text_selection = TextSelection()
        self.clippy = Clippy()

    def stop_redraw(self):
        """Temporarily stop quueue_draw"""
        self._redraw_stop = True

    def restore_redraw(self):
        """restart queue_draw"""
        self._redraw_stop = False
        self.queue_redraw()

    def property_json_filter(self, name, value):
        """fix value that is not json serializable"""
        if value is None:
            return
        if name in ["background", "canvas_color"]:
            value = list(value)
        if name in ["crop_rect"]:
            value = util.rect2dict(value)
        return value

    @property
    def persistent_settings(self):
        """Retrieve persistent settings"""
        info = {k: self.get_property(k) for k in self.persistent_keys}
        info2 = {k: getattr(self, k)
                for k in self.persistent_doc_keys}
        info.update(info2)
        for k, v in info.items():
            nv = self.property_json_filter(k, v)
            if v != nv:
                info[k] = nv
        return info

    @persistent_settings.setter
    def persistent_settings(self, settings):
        """Load save setting for the document"""
        for k, v in settings.items():
            if k in ["background", "canvas_color"]:
                v = Gdk.RGBA(*v)
            elif k == "crop_rect" and v is not None:
                v1 = Gdk.Rectangle()
                for i in v.keys():
                    setattr(v1, i, v[i])
                v = v1

            if k in self.persistent_doc_keys:
                setattr(self, k, v)
            elif k in self.persistent_keys:
                self.set_property(k, v)

    def preload_pages(self, cur_index, preload_num=2):
        doc = self.document
        page_list1 = [cur_index + i for i in range(preload_num)]
        page_list2 = [cur_index - i for i in range(preload_num)]
        page_list = [x for x in page_list1 + page_list2
                if 0 <= x < doc.page_count]
        for i in page_list:
            if self.current_index != cur_index:
                break
            doc.get_page(i)
            yield True
        self.preload_id = 0
        yield False

    # GObject.Property always sent out notify whether the value changed or
    # not. Hence Python Property was used and emit "page-changed" signal
    @property
    def current_index(self):
        return self._current_index

    @current_index.setter
    def current_index(self, x):
        doc = self.document
        if doc is None:
            return
        if not (0 <= x < doc.page_count):
            return
        if self._current_index == x:
            return

        doc.get_page(x)
        old_page = self.current_index
        self._current_index = x
        self.emit("page-changed", old_page, self._current_index)

        # preload some pages
        if self.preload_id != 0:
            GLib.source_remove(self.preload_id)
            self.preload_id = 0
        preloader = self.preload_pages(x)
        self.preload_id = GLib.idle_add(next, preloader)

    def get_page(self, page_num):
        try:
            res = self.document.get_page(page_num)
        except PDFError as e:
            log.error("MuView.get_page failed: {}".format(e))
            res = None
        return res

    def page_bound(self, page_num):
        """Return bound of a give page"""
        page = self.get_page(page_num)
        bound = page.bound()
        if self.rotate % 360 != 0:
            m = fitz.Matrix(1, 1)
            m.prerotate(self.rotate)
            bound.transform(m)
        return bound

    @property
    def current_page(self):
        return self.get_page(self.current_index)

    @property
    def bound(self):
        bound = self.page_bound(self.current_index)
        return bound

    def render_pixbuf(self, page_num, matrix):
        """Render a pdf page to pixbuf"""
        if self.rotate % 360 != 0:
            matrix = matrix_pre_rotate(matrix, self.rotate)
        pix = self.document.render(page_num, matrix)
        return pix

    def setup_widget(self):
        self.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)

        self.dwg = Gtk.DrawingArea()
        self.dwg.set_size_request(100, 100)
        self.dwg.props.expand = False
        self.dwg.props.halign = Gtk.Align.START
        self.dwg.props.valign = Gtk.Align.START
        self.dwg.props.can_focus = True
        self.add(self.dwg)

        emask = self.dwg.get_events()
        self.dwg.add_events(emask |
                Gdk.EventMask.BUTTON_PRESS_MASK |
                Gdk.EventMask.BUTTON_RELEASE_MASK |
                Gdk.EventMask.POINTER_MOTION_MASK
                )
        self.dwg.connect("draw", self.on_draw)
        self.dwg.connect("map", self.on_map)
        self.dwg.connect('button-release-event', self.on_dwg_button_released)
        self.dwg.connect('button-press-event', self.on_dwg_button_pressed)
        self.dwg.connect('motion-notify-event', self.on_dwg_motion_notify)
        self.connect("destroy", self.on_destroy)
        self.connect('key-press-event', self.on_key_pressed)
        self.connect_after("notify", self.on_notify_property)
        self.connect("notify::zoom", self.on_notify_zoom)
        self.connect("notify::rotate", self.on_notify_rotate)
        self.connect("notify::num-columns", self.on_notify_num_columns)
        self.props.hadjustment.connect("changed", self.on_adjustment_changed)
        self.props.vadjustment.connect("changed", self.on_adjustment_changed)

        self.show_all()

    @property
    def path(self):
        """PDF document path"""
        if not self.document:
            return
        return self.document.path

    def open_document(self, path):
        '''Set the document of the view.'''
        self.close_document()

        doc = PDFDoc(path)
        self.document = doc
        #self.update_page_size()
        self.connect('page-changed', self.on_page_changed)

    def close_document(self):
        doc = self.document
        if doc is not None:
            self.disconnect_by_func(self.on_page_changed)
            doc.close()

            # try to release memory, not really work
            del doc
            import gc
            gc.collect()
            self.document = None

    def queue_redraw(self):
        """Redraw cairo surface with current drawing context"""
        if self._redraw_stop:
            return
        log.debug("queue_redraw")
        self.dwg.queue_draw()

    def queue_redraw_area(self, x, y, w, h, margin=0):
        """Redraw cairo surface with current drawing context"""
        if self._redraw_stop:
            return
        log.debug("queue_redraw_area")
        x = max(0, x - margin)
        y = max(0, y - margin)
        self.dwg.queue_draw_area(x, y, w + 2 * margin, h + 2 * margin)

    def on_map(self, wid):
        """ DrawingArea "map" event"""
        wid.disconnect_by_func(self.on_map)

        # create cursors for later use
        if self._cursor_pointer is None:
            gdkwin = wid.props.window
            self._cursor_origin = gdkwin.get_cursor()

            disp = gdkwin.get_display()
            self._cursor_pointer = Gdk.Cursor.new_from_name(disp, "pointer")

    def selected_to_clipboard(self, select_type="clipboard"):
        """Copy selected text to a clipboard"""
        buf = self.selection_text
        if not buf:
            return

        select_type = select_type.lower()
        if select_type == "clipboard":
            self.clippy.clipboard_text = buf
        else:
            self.clippy.primary_text = buf
        return

    def on_dwg_button_pressed(self, wid, evt):
        if not self.props.has_focus:
            self.grab_focus()
        ret = self.dragger.do_button_press(evt)
        return ret

    def on_dwg_button_released(self, wid, evt):
        #print(evt, dir(evt))

        # grab focus if not
        if not self.props.has_focus:
            self.grab_focus()

        ret = self.dragger.do_button_released(evt)
        if ret:
            return ret

        # if over a link, go to the link
        links = self.document.get_link(self.current_index)
        matrix = self.crop_matrix

        if links and evt.button == 1:
            for rect, dest in links:
                r = rect_transform(matrix, rect)
                if r.x0 < evt.x < r.x1 and r.y0 < evt.y < r.y1:
                    self.goto_dest(dest)
                    return True

    def goto_dest(self, linkdest):
        """go to a link destination"""
        if linkdest.kind == fitz.LINK_GOTO:
            self.set_page(linkdest.page)
            if linkdest.lt:
                self.scroll_to(linkdest.lt)
            elif linkdest.rb:
                self.scroll_to(linkdest.rb, True)
        elif linkdest.kind == fitz.LINK_URI:
            Gio.AppInfo.launch_default_for_uri(linkdest.uri, None)

    def toggle_cursor(self, to_pointer):
        """toggle b/w pointer cursor (for link) or plain cursor"""
        gdkwin = self.dwg.props.window
        if not gdkwin:
            return
        cursor = gdkwin.get_cursor()
        to_cursor = (self._cursor_pointer if to_pointer
                else self._cursor_origin)
        if to_cursor != cursor:
            gdkwin.set_cursor(to_cursor)

    @property
    def real_zoom(self):
        """give actually zoom number, always > 0"""
        zoom = self.zoom
        if zoom < 0:
            matrix = self.get_zoom_matrix(zoom)
            zoom = matrix.a
        return zoom

    @functools.lru_cache(CACHE_SIZE_32)
    def _column_rect_from_crop(self, col_idx, num_col,
            col_extra_width, page_width, page_height, crect):
        """Create crop rectangle for the given column id and crop area
        The result is cached.
        """
        if crect:
            x, y, width, height = crect.x, crect.y, crect.width, crect.height
        else:
            x = y = 0
            width = page_width
            height = page_height

        width = width / num_col
        width_extra = width * col_extra_width
        width_real = width + width_extra
        x0 = x + width * col_idx
        if col_idx == num_col - 1:
            x0 -= width_extra
        elif col_idx > 0:
            x0 -= width_extra / 2
        rect = gdk_rect_new(x0, y, width_real, height)
        return rect

    def column_rect(self, col_idx=-1):
        if col_idx < 0:
            col_idx = self.current_column
        rect = self._column_rect_from_crop(col_idx, self.num_columns,
                self.column_extra_width, self.width, self.height,
                self.crop_rect)
        return rect

    @property
    def real_crop(self):
        """Return the real crop rectangle that will cut the final page"""
        if self.num_columns > 1:
            rect = self.column_rect()
        else:
            rect = self.crop_rect
        return rect

    @property
    def crop_matrix(self):
        """get matrix with crop"""
        matrix = zoom_matrix(self.real_zoom)

        crect = self.real_crop
        if crect is not None:
            matrix = matrix_pre_translate(matrix, -crect.x, -crect.y)

        return matrix

    def get_zoom_matrix(self, zoom):
        new_width = self.props.hadjustment.props.page_size
        new_height = self.props.vadjustment.props.page_size
        # maybe called with scrolledwidnow not setup?
        crect = self.real_crop
        if not new_width or not new_height:
            return Identity
        elif crect:
            width = crect.width
            height = crect.height
        else:
            width = self.width
            height = self.height

        matrix = zoom_matrix_with_size(zoom, width, height,
                new_width, new_height)
        return matrix

    def on_dwg_motion_notify(self, wid, evt):

        ret = self.dragger.do_motion_notify(evt)
        if ret:
            return ret

        # test for cursor change over links
        links = self.document.get_link(self.current_index)
        matrix = self.crop_matrix
        if links:
            for rect, dest in links:
                r = rect_transform(matrix, rect)
                if r.x0 < evt.x < r.x1 and r.y0 < evt.y < r.y1:
                    self.toggle_cursor(True)
                    return True
        self.toggle_cursor(False)

    def on_page_changed(self, obj, old_page_num, new_page_num):
        '''Reset page info when document page changed.'''
        if old_page_num == new_page_num:
            return
        self.toggle_cursor(False)  # reset cursor

        self.find_result = None
        self.find_index = 0

        self.update_page_size()

        self.props.vadjustment.props.value = 0
        self.last_read_value = 0

        # reset dragging text select
        self.text_selection.set_page(self.current_page)
        self.dragger.reset()

    def update_page_size(self):
        """Update page dimension information"""
        page = self.current_page
        if page:
            bound = self.bound
            self.width = bound.width
            self.height = bound.height
            self.set_size()

    def set_page(self, page_num):
        """set current displayed page number"""
        if page_num != self.current_index:
            self.current_index = page_num
            self.queue_redraw()

    def set_size(self):
        '''Save page size and set drawing area size to the current page.'''
        zoom = self.zoom
        matrix = self.get_zoom_matrix(zoom)
        crect = self.real_crop
        if crect:
            width = crect.width
            height = crect.height
        else:
            width = self.width
            height = self.height

        dim = matrix_transfer(matrix, width, height)

        width, height = [round(x + 0.5) for x in dim]

        # sometime alloc give smaller size then size request.
        # maybe our own bug?
        w_alloc = self.dwg.get_allocated_width()
        h_alloc = self.dwg.get_allocated_height()

        if width == w_alloc and height == h_alloc:
            return

        self.dwg.set_size_request(width, height)
        self.dwg.get_preferred_size()
        # to silence without calling gtk_widget_get_preferred_width/height()
        self.get_preferred_size()
        self.queue_redraw()

    def on_notify_property(self, gobj, gparam):
        name = gparam.name
        cname = name.replace("-", "_")

        # redraw on property change
        need_redraw = {"apply_background", "background", "canvas_color",
                "crop_rect",
                "indicate_last_read", "indicate_last_read_draw_line",
                "read_indicator_width", "invert_color", "invert_background",
                "zoom"}
        if cname in need_redraw:
            self.queue_redraw()

    def on_notify_zoom(self, gobj, gparam):
        if self.zoom != 1 and abs(self.zoom - 1) < EPSILON:
            self.zoom = 1
            return
        self.set_size()

    def on_notify_rotate(self, gobj, gparam):
        """
        # Force 90 degree rotation
        if self.rotate % 90 != 0:
            self.rotate = (self.rotate // 90) * 90
            return
        """
        self.update_page_size()

    def on_notify_num_columns(self, gobj, gparam):
        self.current_column = 0
        self.set_size()
        self.queue_redraw()

    def on_adjustment_changed(self, wid):
        # page size only mattered for width/page zoom
        if (self.zoom < 0 and
                self.props.vadjustment.props.page_size > 0 and
                self.props.hadjustment.props.page_size > 0):
            if self.document:
                self.set_size()

    def zoom_in(self, more=False):
        zoom = self.real_zoom
        if more:
            zoom_new = zoom + self.zoom_step_more
        else:
            zoom_new = zoom + self.zoom_step
        self.zoom = zoom_new

    def zoom_out(self, more=False):
        zoom = self.real_zoom
        if more:
            zoom_new = zoom - self.zoom_step_more
        else:
            zoom_new = zoom - self.zoom_step
        if zoom_new <= 0:
            zoom_new = zoom
        self.zoom = zoom_new

    def zoom_to_width(self):
        if self.zoom != Zoom.to_width:
            self.zoom = Zoom.to_width

    def zoom_to_page(self):
        if self.zoom != Zoom.to_page:
            self.zoom = Zoom.to_page

    def on_crop_dialog_response(self, dlg, response_id):
        if response_id == Gtk.ResponseType.OK:
            rect = dlg.rect
            if rect_equal(rect, dlg.identity_rect):
                rect = None
            if rect:
                matrix = matrix_invert(dlg.pixbuf.matrix)
                rect = rect_transform(matrix, rect)
            if rect:
                rect = rect_fitz2gdk(rect)
            self.crop_rect = rect
            self.set_size()
            #print(self.crop_rect, self.bound)

    def ask_crop_region(self):
        """popup dialog to ask for crop region"""
        if not self.crop_dialog:
            from . import cropdialog
            self.crop_dialog = cropdialog.CropPixbuf()
            self.crop_dialog.connect("response",
                    self.on_crop_dialog_response)
        cdialog = self.crop_dialog

        screen = self.props.window.get_screen()
        max_width = screen.get_width() * 2 // 3
        max_height = screen.get_height() * 2 // 3

        if self.width > max_width or self.height > max_width:
            matrix = zoom_matrix_with_size(Zoom.to_page, self.width,
                    self.height, max_width, max_height)
        else:
            matrix = Identity

        pixbuf = self.render_pixbuf(self.current_index, matrix)
        pixbuf.matrix = matrix

        rect = self.crop_rect
        if rect is not None:
            rect = rect_gdk2fitz(rect)
        cdialog.set_pixbuf(pixbuf, rect)

        cdialog.show_all()
        cdialog.present()

    def pre_render_page(self, cur_index, matrix):
        """prefetching one page """
        if cur_index == self.current_index:
            page_num = cur_index + 1
            try:
                self.render_pixbuf(page_num, matrix)
            except PDFError as e:
                log.debug("pre_render_page {} failed: {}".format(
                    page_num, e))
        self.pre_render_id = 0
        return False

    def render_page(self, cr):
        '''We can render to a image surface and copy to dwg to speed up.
        @target: surface from cairo.context.get_target()
        '''
        if not self.document:
            return

        matrix = self.get_zoom_matrix(self.zoom)
        pixbuf = self.render_pixbuf(self.current_index, matrix)

        # pre-render one page
        if self.pre_render_id > 0:
            GLib.source_remove(self.pre_render_id)
            self.pre_render_id = 0
        self.pre_render_id = GLib.idle_add(self.pre_render_page,
                self.current_index, matrix)

        crect = self.real_crop
        if crect:
            crop_rect = rect_transform(matrix,
                    rect_gdk2fitz(crect))
            pixbuf = pixbuf.new_subpixbuf(crop_rect.x0, crop_rect.y0,
                    crop_rect.width, crop_rect.height)

        # Paint canvas color
        cr.save()
        cr.set_source_rgba(*list(self.canvas_color))
        cr.paint()
        cr.restore()

        cr.save()
        Gdk.cairo_set_source_pixbuf(cr, pixbuf, 0, 0)
        cr.paint()
        cr.restore()

        self.emit("page-rendered", cr)

    def do_page_rendered(self, cr):
        """Default action for "page-rendered" signal"""
        if not self.skip_draw_background:
            self.draw_background(cr)
            self.draw_invert_color(cr)
        self.draw_links(cr)
        self.draw_links_hint(cr)
        self.draw_find_result(cr)
        self.draw_last_read_indicator(cr)
        self.draw_selection(cr)

    def draw_background(self, cr):
        """
        draw background with deeper shades
        """
        if self.apply_background and self.background is not None:
            bg_color = list(self.background)
            if self.invert_background:
                lit_coef = .80  # 1.0 make bg color a bit too dark
                bg_color = [(1.0 - x) * lit_coef
                        for x in bg_color[:3]] + [bg_color[3]]

            dwg_h = self.dwg.get_allocated_height()
            dwg_w = self.dwg.get_allocated_width()
            cr.save()
            #check_cairo_operator(cr)
            #operator = cairo.OPERATOR_DEST_OVER
            # posible value: MULTIPLY(14), DARKEN(17)
            operator = 14
            cr.set_operator(operator)
            cr.set_source_rgba(*bg_color)
            cr.rectangle(0, 0, dwg_w, dwg_h)
            cr.fill()
            cr.restore()

    def draw_invert_color(self, cr):
        if self.invert_color:
            fg_color = (.80,) * 3
            dwg_h = self.dwg.get_allocated_height()
            dwg_w = self.dwg.get_allocated_width()
            cr.save()
            # posible value: MULTIPLY(14), DARKEN(17), DIFFERENCE(23)
            operator = 23
            cr.set_operator(operator)
            cr.set_source_rgb(*fg_color)
            cr.rectangle(0, 0, dwg_w, dwg_h)
            cr.fill()
            cr.restore()

    def draw_links(self, cr):
        """Draw Links of pages """
        # underline links
        links = self.document.get_link(self.current_index)
        if not links:
            return

        cr.save()
        cr.set_source_rgb(*self.link_color)
        cr.set_operator(cairo.OPERATOR_SOURCE)
        cr.set_line_width(self.link_width)

        crop_matrix = self.crop_matrix

        for rect, dest in links:
            rect = rect_transform(crop_matrix, rect)
            x = rect.x0
            y = rect.y1 + self.link_width
            cr.move_to(x, y)
            cr.line_to(x + rect.width, y)
            cr.stroke()
        cr.restore()

    def draw_links_hint(self, cr):
        """Draw hint for links of page"""
        if not self.hint_links:
            return

        links = self.document.get_link(self.current_index)
        if not links:
            return

        # use 26 alphabets as index to hint links
        digits = math.ceil(math.log(len(links), 26))
        exp26 = [26**o for o in range(digits - 1, 0, -1)]
        ord0 = 0x41  # chr(ord0) == 'A'

        cr.save()
        cr.set_source_rgb(*self.link_color)
        cr.set_operator(cairo.OPERATOR_SOURCE)
        cr.set_line_width(self.link_width)

        crop_matrix = self.crop_matrix

        for i, pair in enumerate(links):
            rect = pair[0]
            rect = rect_transform(crop_matrix, rect)
            hint_msg = chr(ord0 + i % 26)
            if digits > 1:
                hint_msg = "".join([chr(ord0 + i // o)
                    for o in exp26[1 - digits:]]) + hint_msg
            font_size = min(rect.y1 - rect.y0 * .9, 16)
            cr.set_font_size(font_size)
            # xbearing, ybearing, width, height, xadvance, yadvance
            extents = cr.text_extents(hint_msg)
            x = rect.x0 - extents[2] - extents[0]
            y = rect.y1 + self.link_width
            cr.move_to(x, y)
            cr.show_text(hint_msg)
        cr.restore()

    def draw_last_read_indicator(self, cr):
        """Draw last read indicator"""
        sh = self.dwg.get_allocated_height()
        sw = self.dwg.get_allocated_width()
        ph = self.props.vadjustment.props.page_size
        if not self.indicate_last_read or sh <= ph:
            return

        line_width = self.read_indicator_width // 3

        cr.save()
        cr.set_source_rgba(*self.last_read_color)
        cr.set_operator(cairo.OPERATOR_OVER)
        cr.set_line_width(line_width)

        if self.indicate_last_read_draw_line:
            dashes = [line_width * x for x in [4, 4, 1, 4]]
            cr.set_dash(dashes)
            y = self.last_read_value
            x = 1
            width = sw
            cr.move_to(x, y)
            cr.line_to(width, y)
            cr.stroke()
        else:
            y = self.last_read_value + 1
            x = self.props.hadjustment.props.value + 1
            iwidth = self.read_indicator_width // 2
            cr.move_to(x, y - iwidth)
            cr.line_to(x, y + iwidth)
            cr.line_to(x + iwidth * 2, y)
            cr.close_path()
            cr.fill()
        cr.restore()

    @property
    def selection_text(self):
        dragger = self.dragger
        if dragger.start is None or dragger.end is None:
            return

        crop_matrix = self.crop_matrix
        uncrop_matrix = matrix_invert(crop_matrix)

        mouse_rect = rect_transform(uncrop_matrix,
                *(dragger.start + dragger.end))

        res = self.text_selection.copy_selection(mouse_rect)
        return res

    def draw_selection(self, cr):
        """Draw mouse selection"""
        dragger = self.dragger
        if dragger.start is None or dragger.end is None:
            if dragger.last_select_rects:
                dragger.last_select_rects = None
            return

        crop_matrix = self.crop_matrix
        uncrop_matrix = matrix_invert(crop_matrix)

        mouse_rect = rect_transform(uncrop_matrix,
                *(dragger.start + dragger.end))

        hilight_rects = self.text_selection.hilight_selection(mouse_rect)

        if len(hilight_rects) < 1:
            return

        transformed_rects = []
        cr.save()
        cr.reset_clip()
        cr.set_source_rgb(1.0, 1.0, 1.0)
        # CAIRO_OPERATOR_DIFFERENCE = 23
        cr.set_operator(23)
        for rect_h in hilight_rects:
            rect = rect_transform(crop_matrix, rect_h)
            cr.rectangle(rect.x0, rect.y0, rect.width, rect.height)
            transformed_rects.append(rect)
        cr.fill()
        cr.restore()
        dragger.last_select_rects = transformed_rects

    def draw_find_result(self, cr):
        """ high light search results """
        if not self.find_result:
            return
        cr.save()
        #print('draw', self.find_result)
        for rect in self.find_result:
            #rect = self.find_result[self.find_index]
            cr.set_operator(cairo.OPERATOR_DEST_OVER)
            cr.set_source_rgb(*self.highlight_color)
            cr.rectangle(rect.x1, self.height - rect.y2,
                    rect.x2 - rect.x1, rect.y2 - rect.y1)
            cr.fill()
        cr.restore()

    def on_draw(self, widget, cr):
        """on DrawingArea "draw" event"""""
        page = self.current_page
        if not page:
            return

        self.render_page(cr)

    @property
    def max_hadj(self):
        hadj = self.props.hadjustment.props
        maxh = hadj.upper - hadj.page_size  # Max size can set adj.value
        return maxh

    @property
    def max_vadj(self):
        vadj = self.props.vadjustment.props
        maxv = vadj.upper - vadj.page_size  # Max size can set adj.value
        return maxv

    def scroll_page(self, step=1, page_step=False):
        doc = self.document
        vadj = self.props.vadjustment.props
        pinc = vadj.page_increment if page_step else vadj.step_increment
        pinc = pinc * step

        # mark last read position
        self.last_read_value = vadj.value
        if step > 0:
            self.last_read_value += vadj.page_size

        last_read_paged = 0  # after page changed
        if step < 0:
            last_read_paged = vadj.upper - self.read_indicator_width

        val = vadj.value + pinc
        #print(vadj.value, self.max_vadj, val)
        next_col = self.current_column + step
        if vadj.value < self.max_vadj and step > 0:
            vadj.value = min(self.max_vadj, max(0.0, val))
            self.queue_redraw()
        elif vadj.value > 0 and step < 0:
            vadj.value = min(self.max_vadj, max(0.0, val))
            self.queue_redraw()
        elif self.num_columns > 1 and 0 <= next_col < self.num_columns:
            self.current_column = next_col
            vadj.value = 0.0 if step > 0 else self.max_vadj
            self.last_read_value = last_read_paged
            self.queue_redraw()
        elif 0 <= self.current_index + step < doc.page_count:
            self.set_page(self.current_index + step)
            self.current_column = 0 if step > 0 else self.num_columns - 1
            vadj.value = 0.0 if step > 0 else self.max_vadj
            self.last_read_value = last_read_paged

    def scroll_right(self):
        hadj = self.props.hadjustment.props
        if hadj.value < self.max_hadj:
            hadj.value = min(hadj.value + hadj.step_increment, self.max_hadj)

    def scroll_left(self):
        hadj = self.props.hadjustment.props
        if hadj.value > 0.0:
            hadj.value = max(hadj.value - hadj.step_increment, 0.0)

    def scroll_to(self, point, is_right_bottom=False):
        """Scroll the view to the point"""
        if self.rotate % 360 != 0:
            m = fitz.Matrix(1, 1)
            m.prerotate(self.rotate)
            point.transform(m)

        percent_h = point.x / self.bound.width
        percent_v = point.y / self.bound.height

        def adj_to_percent(adj, percent):
            """Set adjustment so that percent is visible"""
            ap = adj.props
            val = (ap.upper - ap.lower) * percent
            if not is_right_bottom:
                ap.value = val
            else:
                ap.value = val - ap.page_size
            return

        adj_to_percent(self.props.hadjustment, percent_h)
        adj_to_percent(self.props.vadjustment, percent_v)

    def scroll_home(self):
        self.set_page(0)
        vadj = self.props.vadjustment.props
        vadj.value = 0.0
        if self.num_columns > 1:
            self.current_column = 0

    def scroll_end(self):
        self.set_page(self.document.page_count - 1)
        vadj = self.props.vadjustment.props
        vadj.value = self.max_vadj
        if self.num_columns > 1:
            self.current_column = self.num_columns - 1

    def on_key_pressed(self, widget, event):
        if not self.handle_keyevent:
            return

        ret = True
        kname = Gdk.keyval_name(event.keyval)
        #print(kname)
        if kname in set(['space', 'Page_Down', 'Down']):
            self.scroll_page(page_step=kname != "Down")
        elif kname in set(['Page_Up', 'Up']):
            self.scroll_page(step=-1, page_step=kname != "Up")
        elif kname in set(['Right']):
            self.scroll_right()
        elif kname in set(['Left']):
            self.scroll_left()
        elif kname in set(['Home']):
            self.scroll_home()
        elif kname in set(['End']):
            self.scroll_end()
        elif kname in set(['KP_Add', 'equal', 'plus']):
            self.zoom_in()
        elif kname in set(['KP_Subtract', 'minus']):
            self.zoom_out()
        elif kname in set(['w']):
            self.zoom_to_width()
        elif kname in set(['p']):
            self.zoom_to_page()
        elif kname in set(['c']):
            self.ask_crop_region()
        else:
            ret = False

        return ret

    def jump_to_find(self):
        '''Present the search result to user, scroll to display.'''
        hadj = self.get_hadjustment()
        vadj = self.get_vadjustment()
        if self.find_result:
            zoom = self.zoom
            hsize, vsize = hadj.page_size, vadj.page_size
            rect = self.find_result[self.find_index]
            x, y = rect.x1, self.height - rect.y2
            x2, y2 = rect.x2, self.height - rect.y1
            x, y = x * zoom, y * zoom
            x2, y2 = x2 * zoom, y2 * zoom
            if (not
                    (hadj.value < x < (hadj.value + hsize) and
                        hadj.value < x2 < (hadj.value + hsize) and
                        vadj.value < y2 < (vadj.value + vsize) and
                        vadj.value < y < (vadj.value + vsize)
                        )):
                # Max size can set adj.value
                maxh = hadj.upper - hadj.page_size
                maxv = vadj.upper - vadj.page_size
                xval = x - hsize * self.jump_ratio
                yval = y - vsize * self.jump_ratio
                xval = min(maxh, xval); xval = max(0.0, xval)
                yval = min(maxv, yval); yval = max(0.0, yval)
                hadj.value, vadj.value = xval, yval
        self.dwg.queue_draw()

    def find_first(self, search_text):
        #page = self.current_page # cannot search from start.
        page = self.document.get_page(self.current_index)
        self.find_result = page.find_text(search_text)
        self.find_index = 0
        # for r in self.find_result: print(r.x1, r.x2, r.y1, r.y2)
        self.jump_to_find()

    def find_next(self):
        if self.find_result:
            # cycling
            if self.find_index >= len(self.find_result) - 1:
                self.find_index = 0
            else:
                self.find_index = self.find_index + 1
        self.jump_to_find()

    def find_previous(self):
        if self.find_result:
            if self.find_index > 0:
                self.find_index -= 1
            else:
                self.find_index = len(self.find_result) - 1
        self.jump_to_find()

    def do_action_200_msecond(self):
        """do some action every 200 milliseconds"""
    def do_action_1_second(self):
        """do some action every second"""

    def on_destroy(self, wid):
        """on "destroy" event"""
        self.close_document()

def main():
    def set_stdio_encoding(enc=NATIVE):
        import codecs; stdio = ["stdin", "stdout", "stderr"]
        for x in stdio:
            obj = getattr(sys, x)
            if not obj.encoding: setattr(sys, x, codecs.getwriter(enc)(obj))
    set_stdio_encoding()

    log_level = log.INFO
    log.basicConfig(format="%(levelname)s>> %(message)s", level=log_level)

    fname = sys.argv[1]
    doc = PDFDoc(fname)
    view = MuView()
    view.set_document(doc)  # setup widget
    view.zoom = 3

    w = Gtk.Window()
    w.resize(600, 700)
    w.add(view)
    w.show_all()  # main window.
    #view.find_first(sys.argv[2])        # search words

    if doc.metadata["title"]:
        w.props.title = doc.metadata["title"]
    icon = doc.page_icon(0)
    if icon:
        w.props.icon = icon

    def on_page_changed(*args):
        log.debug('Page Chaged: {}'.format(args))
    #doc.connect('page-changed', on_page_changed)
    w.connect("delete-event", Gtk.main_quit)
    Gtk.main()

if __name__ == '__main__':
    import signal; signal.signal(signal.SIGINT, signal.SIG_DFL)
    main()

