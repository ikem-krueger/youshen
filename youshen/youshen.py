#!/usr/bin/python3
# vim:fileencoding=utf-8:sw=4:et
'''
 * Copyright (C) 2009-2016, Mozbugbox
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street - Fifth Floor, Boston, MA 02110-1301,
 * USA.

Two Classes:
    PDFDoc: The document model
    MuView: The view of the document.
'''
from __future__ import print_function, unicode_literals, absolute_import, division

PAGE_CACHE_SIZE = 16

import sys
import os
import io
import logging
import json
import bisect
import functools
import collections

# fitz has to be imported before Gtk, Gdk. Else segfault happens
import fitz
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import GObject, GLib, Gtk, Gdk, Gio

from . import const
from . import util
from . import muview
from . import history
from . import actions

WORKSPACE_WIDTH = -1
WORKSPACE_HEIGHT = -1

NATIVE = sys.getfilesystemencoding()

def hide_window(wobj, *args):
    """Hide a window widget"""
    wobj.hide()
    return True

def clear_children(container):
    """Remove all the children of a container"""
    def remove_child(kid, papa):
        papa.remove(kid)
    container.foreach(remove_child, container)

def install_ui_object(obj, uiobj, widget_names):
    for name in widget_names:
        gobj = uiobj.get_object(name)
        attr_name = name.replace("-", "_")
        setattr(obj, attr_name, gobj)

def setup_background_palette(wid):
    """Add some color suitable for reading to the color selector"""
    row_len = 3
    palette_ls = [const.default_palette, const.default_gray_palette,
            const.background_palette]
    for i, palette in enumerate(palette_ls):
        rgba_ls = [util.rgba_parse(x) for x in palette]
        if i in [0]:
            orient, stride = Gtk.Orientation.VERTICAL, row_len,
        else:
            orient = Gtk.Orientation.HORIZONTAL
            stride = len(const.default_palette) // row_len
        wid.add_palette(orient, stride, rgba_ls)

class DocumentControl(actions.ActionControlBase):
    """Action handlers for DocumentWindow"""
    @property
    def document_window(self):
        return self.action_map.document_window

    @property
    def doc_view(self):
        return self.document_window.doc_view

    def page_down(self, action, param):
        self.doc_view.scroll_page(step=1, page_step=True)
    def step_down(self, action, param):
        self.doc_view.scroll_page(step=1, page_step=False)
    def page_up(self, action, param):
        self.doc_view.scroll_page(step=-1, page_step=True)
    def step_up(self, action, param):
        self.doc_view.scroll_page(step=-1, page_step=False)
    def page_right(self, action, param):
        self.doc_view.scroll_right()
    def page_left(self, action, param):
        self.doc_view.scroll_left()

    def doc_or_tree_view_page_down(self, action, param):
        """Take page down action depends on focused Widget"""
        focused = self.document_window.window_main.get_focus()
        if isinstance(focused, Gtk.TreeView):
            focused.emit("move-cursor", Gtk.MovementStep.PAGES, 1)
        else:
            self.do_action("page_down")

    def doc_or_tree_view_page_up(self, action, param):
        focused = self.document_window.window_main.get_focus()
        if isinstance(focused, Gtk.TreeView):
            focused.emit("move-cursor", Gtk.MovementStep.PAGES, -1)
        else:
            self.do_action("page_up")

    def doc_or_tree_view_step_down(self, action, param):
        focused = self.document_window.window_main.get_focus()
        if isinstance(focused, Gtk.TreeView):
            focused.emit("move-cursor", Gtk.MovementStep.DISPLAY_LINES, 1)
        else:
            self.do_action("step_down")

    def doc_or_tree_view_step_up(self, action, param):
        focused = self.document_window.window_main.get_focus()
        if isinstance(focused, Gtk.TreeView):
            focused.emit("move-cursor", Gtk.MovementStep.DISPLAY_LINES, -1)
        else:
            self.do_action("step_up")

    def doc_or_tree_view_left(self, action, param):
        """Take left arrow key action depends on focused Widget"""
        focused = self.document_window.window_main.get_focus()
        if isinstance(focused, Gtk.TreeView):
            # collapse row on treeview
            path, col = focused.get_cursor()
            if path:
                store = focused.get_model()
                miter = store.get_iter(path)
                if (not store.iter_has_child(miter) or
                        not focused.row_expanded(path)):
                    res = path.up()
                    if not res:
                        path = None
            if path:
                focused.collapse_row(path)
        else:
            self.do_action("page_left")

    def doc_or_tree_view_right(self, action, param):
        """Take right arrow key action depends on focused Widget"""
        focused = self.document_window.window_main.get_focus()
        if isinstance(focused, Gtk.TreeView):
            # expand row on treeview
            path, col = focused.get_cursor()
            if path:
                store = focused.get_model()
                miter = store.get_iter(path)
                if store.iter_has_child(miter):
                    focused.expand_row(path, False)
        else:
            self.do_action("page_right")

    def go_home(self, action, param):
        self.doc_view.scroll_home()
    def go_end(self, action, param):
        self.doc_view.scroll_end()
    def zoom_in(self, action, param):
        self.doc_view.zoom_in()
    def zoom_out(self, action, param):
        self.doc_view.zoom_out()
    def zoom_in_more(self, action, param):
        self.doc_view.zoom_in(True)
    def zoom_out_more(self, action, param):
        self.doc_view.zoom_out(True)
    def zoom_origin(self, action, param):
        if self.doc_view.zoom != 1:
            self.doc_view.zoom = 1
    def zoom_to_width(self, action, param):
        self.doc_view.zoom_to_width()
    def zoom_to_page(self, action, param):
        self.doc_view.zoom_to_page()

    def rotate_clockwise(self, action, param):
        """rotate PDF by 90 degree"""
        deg = (self.doc_view.rotate + 90) % 360
        if deg != self.doc_view.rotate:
            self.doc_view.rotate = deg

    def toggle_invert_color(self, action, param):
        val = param.get_boolean()
        if self.doc_view.invert_color != val:
            self.doc_view.invert_color = val

    def toggle_invert_background(self, action, param):
        val = param.get_boolean()
        if self.doc_view.invert_background != val:
            self.doc_view.invert_background = val

    def toggle_skip_draw_background(self, action, param):
        dv = self.doc_view
        dv.skip_draw_background = not dv.skip_draw_background
        dv.queue_redraw()

    def toggle_scroll_mark(self, action, param):
        val = param.get_boolean()
        self.doc_view.props.indicate_last_read = val
    def toggle_scroll_mark_draw_line(self, action, param):
        val = param.get_boolean()
        self.doc_view.props.indicate_last_read_draw_line = val

    def set_num_columns(self, action, param):
        val = param.get_string()
        if self.doc_view.props.num_columns != int(val):
            self.doc_view.props.num_columns = int(val)

    def crop_page(self, action, param):
        self.doc_view.ask_crop_region()

    def go_backward(self, action, param):
        hist = self.document_window.history
        val = hist.undo(self.doc_view.current_index)
        if val is not None:
            self.doc_view.set_page(val)

    def go_forward(self, action, param):
        hist = self.document_window.history
        val = hist.redo(self.doc_view.current_index)
        if val is not None:
            self.doc_view.set_page(val)

    def selected_to_clipboard(self, action, param):
        self.doc_view.selected_to_clipboard()

    def clear_history(self, action, param):
        hist = self.document_window.history
        hist.clear()

    def show_properties(self, action, param):
        """Show document properties"""
        self.document_window.show_properties()

    def close_doc(self, action, param):
        self.document_window.window_main.close()

    def toggle_show_sidebar(self, action, param):
        val = param.get_boolean()
        dw = self.document_window
        dw.show_sidebar = val

    def save_page_as(self, action, param):
        """Save page as image dialog"""
        dlg_fopen = Gtk.FileChooserDialog("Save Page As...",
                self.action_map,
                Gtk.FileChooserAction.SAVE, [
                    "_Cancel", Gtk.ResponseType.CANCEL,
                    "_Save", Gtk.ResponseType.ACCEPT,
                    ])
        dlg_fopen.props.mnemonics_visible = True

        filter_patterns = [
                ("*.png", ["*.png"]),
                ("All", ["*.*"]),
            ]

        for name, patterns in filter_patterns:
            ff = Gtk.FileFilter()
            ff.set_name(name)
            patterns_u = [x.upper() for x in patterns if x.islower()]
            if len(patterns_u) > 0:
                patterns.extend(patterns_u)
            for p in patterns:
                ff.add_pattern(p)
            dlg_fopen.add_filter(ff)

        def _on_open_response(dlg, res):
            if (res == Gtk.ResponseType.ACCEPT):
                fname = dlg.get_filename()

                doc_view = self.doc_view
                matrix = doc_view.get_zoom_matrix(doc_view.zoom)
                pixbuf = doc_view.render_pixbuf(doc_view.current_index,
                        matrix)
                img_type = os.path.splitext(fname)[1].lstrip(".")
                if img_type == "jpg":
                    img_type = "jpeg"
                pixbuf.savev(fname, img_type, (), ())

            dlg.destroy()

        dlg_fopen.connect("response", _on_open_response)
        dlg_fopen.show_all()
        dlg_fopen.present()

class DocumentWindow(GObject.GObject):
    show_sidebar = GObject.Property(type=bool, default=True)
    persistent_keys = ["show_sidebar"]
    def __init__(self, app, path):
        super().__init__()
        self.doc_view = None
        self.is_maximized = False
        self.is_fullscreen = False
        self.document_control = None

        self.app = app
        self.gapp = app.gapp
        self.history = history.History()

        self.load_ui()
        GLib.idle_add(self.open_doc, path)

    def property_json_filter(self, name, value):
        """fix value that is not json serializable"""
        if value is None:
            return
        return value

    @property
    def persistent_settings(self):
        """Retrieve persistent settings"""
        info = {k: self.get_property(k) for k in self.persistent_keys}
        for k, v in info.items():
            nv = self.property_json_filter(k, v)
            if v != nv:
                info[k] = nv
        info["history"] = self.history.state
        return info

    @persistent_settings.setter
    def persistent_settings(self, settings):
        """Load save setting for the document"""
        for k, v in settings.items():
            if k in self.persistent_keys:
                self.set_property(k, v)

    def load_settings(self, path):
        """Load document setting from disk"""
        settings = self.app.db.document_setting_get(path)
        settings = {x: json.loads(y) for x, y in settings.items()}
        return settings

    def save_settings(self, **settings):
        """save document setting to disk"""
        if self.app.db is None:
            log.debug("Failed save_settings. self.app.db is None")
            return
        settings = {x: json.dumps(y) for x, y in settings.items()}
        self.app.db.document_setting_set(self.doc_view.path, **settings)
        return settings

    def on_save_property(self, obj, gparam):
        # Save doc settings on change
        name = gparam.name
        local_names = obj.persistent_keys
        cname = name.replace("-", "_")
        if cname not in local_names:
            return
        value = obj.get_property(name)
        value = obj.property_json_filter(cname, value)
        self.save_settings(**{cname: value})

    def on_notify_background(self, obj, gparam):
        props = self.colorbutton_background.props
        if props.rgba != obj.background:
            props.rgba = obj.background

    def on_notify_canvas_color(self, obj, gparam):
        props = self.colorbutton_canvas.props
        if props.rgba != obj.canvas_color:
            props.rgba = obj.canvas_color

    def on_notify_apply_background(self, obj, gparam):
        props = self.checkbox_background.props
        if props.active != obj.apply_background:
            props.active = obj.apply_background

    def on_notify_invert_color(self, obj, gparam):
        dc = self.document_control
        action = dc.get_action("toggle_invert_color")
        if action:
            val = GLib.Variant.new_boolean(self.doc_view.invert_color)
            action.set_state(val)

    def on_notify_invert_background(self, obj, gparam):
        dc = self.document_control
        action = dc.get_action("toggle_invert_background")
        if action:
            val = GLib.Variant.new_boolean(self.doc_view.invert_background)
            action.set_state(val)

    def on_notify_indicate_last_read(self, obj, gparam):
        dc = self.document_control
        action = dc.get_action("toggle_scroll_mark")
        if action:
            val = GLib.Variant.new_boolean(self.doc_view.indicate_last_read)
            action.set_state(val)
            self.sync_indicate_last_read_actions()

    def on_notify_indicate_last_read_draw_line(self, obj, gparam):
        action = self.document_control.get_action(
                "toggle_scroll_mark_draw_line")
        if action:
            val = GLib.Variant.new_boolean(
                    self.doc_view.indicate_last_read_draw_line)
            action.set_state(val)

    def on_notify_num_columns(self, obj, gparam):
        action = self.document_control.get_action("set_num_columns")
        if action:
            val = GLib.Variant.new_string(str(self.doc_view.num_columns))
            action.set_state(val)

    def on_notify_show_sidebar(self, obj, gparam):
        action = self.document_control.get_action("toggle_show_sidebar")
        if action:
            val = GLib.Variant.new_boolean(self.show_sidebar)
            action.set_state(val)

        self.grid_sidebar.props.visible = self.show_sidebar
        if self.show_sidebar:
            self.set_content_to_page(self.doc_view.current_index)

    def on_notify_paned_main_position(self, obj, gparam):
        pos = self.paned_main.props.position
        self.save_settings(paned_main_position=pos)

    def show_window(self, settings):
        doc_view = self.doc_view
        win = self.window_main
        if "window_width" in settings:
            win.set_default_size(settings["window_width"],
                    settings["window_height"])
            win.resize(settings["window_width"],
                    settings["window_height"])
            win.show()

        def _idle_run():
            if "window_width" not in settings:
                win.realize()
                width_win = win.get_allocated_width()
                height_win = win.get_allocated_height()

                width_dview = doc_view.get_allocated_width()
                height_dview = doc_view.get_allocated_height()
                width_border = width_win - width_dview
                height_border = height_win - height_dview
                width = doc_view.width + width_border
                height = doc_view.height + height_border
                if width < WORKSPACE_WIDTH:
                    self.paned_main.props.position = width_border

                width = min(width, WORKSPACE_WIDTH)
                height = min(height, WORKSPACE_HEIGHT)

                win.set_default_size(width, height)
                win.resize(width, height)
                win.show()

            paned_pos = settings.get("paned_main_position", None)
            if paned_pos:
                self.paned_main.props.position = paned_pos

            if settings.get("window_maximized", False):
                win.maximize()
            if settings.get("window_fullscreen", False):
                win.fullscreen()
            win.show()
            win.present()

        GLib.idle_add(_idle_run)

    def load_table_of_content(self):
        toc = self.doc_view.document.table_of_content
        if not toc:
            return
        store = self.treestore_content
        # map page number to model iter
        page_to_path = collections.defaultdict(list)
        store.page_to_path = page_to_path

        parents = [None]
        level_now = 1
        last_iter = None
        for level, title, page_num, destlink in toc:
            log.debug(f"{destlink=}, {page_num=}")
            break
        for level, title, page_num, destlink in toc:
            page_num -= 1  # we really need document page_num
            if level > level_now:
                level_now = level
                parents.append(last_iter)
            elif level < level_now:
                del parents[level - level_now:]
                level_now = level
            parent = parents[-1]
            destobj = muview.GData(destlink)
            try:
                last_iter = store.append(parent, [title, page_num, destobj])
            except UnicodeEncodeError:
                title = title.encode("utf8", "replace").decode("utf8")
                last_iter = store.append(parent, [title, page_num, destobj])
            path = store.get_path(last_iter)
            page_to_path[page_num].append(path.to_string())

    def sync_indicate_last_read_actions(self):
        dc = self.document_control
        line_action = dc.get_action("toggle_scroll_mark_draw_line")
        if line_action:
            line_action.set_enabled(self.doc_view.indicate_last_read)

    def open_doc(self, path):
        win = self.window_main
        doc_view = self.doc_view

        settings = self.load_settings(path)
        self.show_window(settings)

        doc_view.stop_redraw()

        doc_view.connect("notify", self.on_save_property)
        doc_view.connect("notify::canvas-color", self.on_notify_canvas_color)
        doc_view.connect("notify::background", self.on_notify_background)
        doc_view.connect("notify::apply-background",
                self.on_notify_apply_background)
        doc_view.connect("notify::invert-color",
                self.on_notify_invert_color)
        doc_view.connect("notify::invert-background",
                self.on_notify_invert_background)
        doc_view.connect("notify::indicate-last-read",
                self.on_notify_indicate_last_read)
        doc_view.connect("notify::indicate-last-read-draw-line",
                self.on_notify_indicate_last_read_draw_line)
        doc_view.connect("notify::num-columns",
                self.on_notify_num_columns)
        doc_view.open_document(path)
        doc = doc_view.document

        self.connect("notify", self.on_save_property)
        self.connect("notify::show-sidebar", self.on_notify_show_sidebar)

        self.colorbutton_background.connect("color-activated",
                self.on_colorbutton_background_color_activated)

        self.colorbutton_canvas.connect("color-activated",
                self.on_colorbutton_canvas_color_activated)

        self.spinbutton_page.set_range(1, doc.page_count)
        self.spinbutton_page.set_increments(1, 10)


        if "canvas_color" not in settings:
            doc_view.canvas_color = self.colorbutton_canvas.get_rgba()
        if "background" not in settings:
            doc_view.background = self.colorbutton_background.get_rgba()
        if "apply_background" not in settings:
            doc_view.apply_background = False

        doc_view.persistent_settings = settings
        self.persistent_settings = settings

        def _delay_load():
            if doc.metadata["title"]:
                title = doc.metadata["title"]
            else:
                title = os.path.basename(path)

            win.props.title = title
            self.window_properties.props.title = "Properties: " + title
            self.headerbar_main.props.title = title
            icon = doc_view.document.page_icon(0)
            if icon:
                win.props.icon = icon
            self.sync_indicate_last_read_actions()

        GLib.timeout_add(500, _delay_load)

        if "history" in settings:
            self.history.state = settings["history"]

        doc_view.queue_redraw()
        doc_view.grab_focus()
        self.label_page_count.props.label = " of {}".format(doc.page_count)

        doc_view.restore_redraw()
        GLib.idle_add(self.load_table_of_content)

    def on_page_changed(self, wid, old_page, new_page):
        self.spinbutton_page.props.value = new_page + 1
        self.save_settings(current_index=new_page)
        self.set_content_to_page(new_page)
        self.history.on_page_changed(old_page, new_page)

    def set_content_to_page(self, page_index):
        """ set cursor in treeview-content """
        store = self.treestore_content
        page_to_path = getattr(store, "page_to_path", None)
        if page_to_path is not None:
            pages = sorted(page_to_path.keys())
            if page_index in page_to_path:
                new_paths = page_to_path[page_index]
            else:
                # we want previous item in table of content
                insert_index = bisect.bisect_right(pages, page_index) - 1
                if insert_index >= len(pages):
                    insert_index = len(pages) - 1
                insert_at = pages[insert_index]
                new_paths = page_to_path[insert_at]

            treev = self.treeview_content
            path, col = treev.get_cursor()
            if path:
                path_str = path.to_string()
            if path is None or path_str not in new_paths:
                p = Gtk.TreePath.new_from_string(new_paths[0])

                treev.expand_to_path(p)  # need to expand before set_cursor
                treev.scroll_to_cell(p, None, False, False, False)

                block_func = self.on_treeview_content_cursor_changed
                treev.handler_block_by_func(block_func)
                treev.set_cursor(p, None, False)
                treev.handler_unblock_by_func(block_func)

    def on_spinbutton_page_value_changed(self, wid):
        val = int(self.spinbutton_page.props.value)
        self.doc_view.set_page(val - 1)

    def on_checkbox_background_toggled(self, wid):
        self.doc_view.apply_background = wid.props.active
        self.colorbutton_background.sensitive = wid.props.active

    def on_colorbutton_background_color_set(self, wid):
        self.doc_view.background = wid.get_rgba()

    def on_colorbutton_canvas_color_set(self, wid):
        self.doc_view.canvas_color = wid.get_rgba()

    def on_colorbutton_background_color_activated(self, wid, color):
        self.doc_view.background = color
        self.doc_view.queue_redraw()

    def on_colorbutton_canvas_color_activated(self, wid, color):
        self.doc_view.canvas = color
        self.doc_view.queue_redraw()

    def on_window_main_size_allocate(self, wid, allocation):
        if not (self.is_maximized or self.is_fullscreen):
            size = self.window_main.get_size()
            self.save_settings(window_width=size.width,
                    window_height=size.height)

    def on_window_main_window_state_event(self, wid, evt):
        is_max = evt.new_window_state & Gdk.WindowState.MAXIMIZED != 0
        is_full = evt.new_window_state & Gdk.WindowState.FULLSCREEN != 0

        if self.is_maximized != is_max or self.is_fullscreen != is_full:
            self.is_maximized = is_max
            self.is_fullscreen = is_full
            self.save_settings(window_maximized=is_max,
                    window_fullscreen=is_full)

    def on_treeview_content_cursor_changed(self, treev, *args):
        store = treev.get_model()
        path, col = treev.get_cursor()
        if path is None:
            return
        miter = store.get_iter(path)
        row = store[miter]
        # title, page_num, GData.data=destlink
        destlink = row[2].data
        if destlink["kind"] in [fitz.LINK_GOTO, fitz.LINK_NAMED]:
            if row[1] != self.doc_view.current_index:
                self.doc_view.set_page(row[1])
            if "to" in destlink:
                self.doc_view.scroll_to(destlink["to"])

    def on_button_close_sidebar_clicked(self, wid):
        self.document_control.change_action_state("toggle_show_sidebar",
                False)

    def on_button_go_previous_clicked(self, wid):
        val = int(self.spinbutton_page.props.value)
        self.doc_view.set_page(val - 2)

    def on_button_go_next_clicked(self, wid):
        val = int(self.spinbutton_page.props.value)
        self.doc_view.set_page(val)

    def update_history_buttons(self, history):
        return

    def on_history_changed(self, hist, op):
        self.update_history_buttons(hist)
        if op != "state":
            self.save_settings(history=hist.state)

    def show_properties(self):
        meta_keys = [
                'title',
                'author',
                'location',
                'creation Date',
                'mod Date',
                'subject',
                'creator',
                'producer',
                'format',
                'encryption',
                'keywords',
                ]
        doc = self.doc_view.document
        metadata = doc.metadata
        keys = metadata.keys()
        norm_meta_keys = [x.replace(" ", "") for x in meta_keys]
        for k in keys:
            if k not in norm_meta_keys:
                meta_keys.append(k)

        clear_children(self.grid_metadata)
        for i, key in enumerate(meta_keys):
            mkey = key.replace(" ", "")
            val = metadata.get(mkey, None)
            if key == 'location' and val is None:
                val = doc.path
            elif mkey in ['creationDate', "modDate"] and val.startswith("D:"):
                val = util.pdf_time2datetime(val).isoformat()

            label = Gtk.Label(key.title() + ":")
            content = Gtk.Label(str(val))
            label.props.xalign = 0.0
            label.props.yalign = 0.0
            content.props.xalign = 0.0
            content.props.yalign = 0.0
            content.props.wrap = True
            content.props.selectable = True
            self.grid_metadata.attach(label, 0, i, 1, 1)
            self.grid_metadata.attach(content, 1, i, 1, 1)
        self.window_properties.show_all()

    def load_ui(self):
        """load UI"""
        # widgets fetched as attribute for self
        ui_widget_names = [
                "window-main",
                "grid-main-top",
                "grid-sidebar",
                "headerbar-main",
                "paned-main",
                "spinbutton-page",
                "label-page-count",
                "colorbutton-canvas",
                "colorbutton-background",
                "checkbox-background",
                "treeview-content",
                "treestore-content",
                "combobox-sidebar",
                "button-go-previous",
                "button-go-next",
                "menubutton-main",

                # properties window
                "window-properties",
                "grid-metadata",
                ]

        pkg_dir = os.path.dirname(__file__)
        ui_path = os.path.join(pkg_dir, 'data', const.UI_FILE)
        self.ui_xml = Gtk.Builder.new_from_file(ui_path)
        install_ui_object(self, self.ui_xml, ui_widget_names)

        # Work around bug of headerbar for Glade before v3.20
        p = self.headerbar_main.get_parent()
        if p == self.grid_main_top:
            p.remove(self.headerbar_main)
            self.window_main.set_titlebar(self.headerbar_main)

        self.window_main.document_window = self
        self.window_main.props.application = self.gapp
        self.document_control = DocumentControl(self.window_main)

        self.doc_view = muview.MuView()
        self.doc_view.handle_keyevent = False
        self.doc_view.connect("page-changed", self.on_page_changed)
        self.paned_main.pack2(self.doc_view, True, False)
        self.paned_main.connect("notify::position",
                self.on_notify_paned_main_position)

        self.ui_xml.connect_signals(self)
        self.window_main.set_no_show_all(False)
        self.menubutton_main.props.menu_model = self.app.win_menu
        self.menubutton_main.props.use_popover = False

        self.history.connect("changed", self.on_history_changed)
        self.window_properties.connect("delete-event",
                hide_window)
        self.window_properties.props.transient_for = self.window_main

        def _delay_load():
            from . import cropdialog
            cdialog = cropdialog.CropPixbuf(parent=self.window_main)
            self.doc_view.crop_dialog = cdialog
            cdialog.connect("response",
                        self.doc_view.on_crop_dialog_response)

            from .treematcher import tree_matcher
            self.treeview_content.set_search_equal_func(
                    tree_matcher.treeview_equal_func)
            setup_background_palette(self.colorbutton_canvas)
            setup_background_palette(self.colorbutton_background)

        GLib.idle_add(_delay_load)

    def has_path(self, path):
        res = path == self.doc_view.path
        return res

    def present(self):
        self.window_main.present()

    def on_window_main_delete_event(self, wid, evt):
        self.close()

    def close(self):
        settings = self.doc_view.persistent_settings
        settings = self.save_settings(**settings)

        settings = self.persistent_settings
        settings = self.save_settings(**settings)

class ApplicationControl(actions.ActionControlBase):
    @property
    def application(self):
        return self.action_map.application

    def quit(self, action, param):
        self.application.quit()

    def open_file_dialog(self, action, param):
        """Open document file dialog"""
        dlg_fopen = Gtk.FileChooserDialog("Open Document File",
                None,
                Gtk.FileChooserAction.OPEN, [
                    "_Cancel", Gtk.ResponseType.CANCEL,
                    "_Open", Gtk.ResponseType.ACCEPT,
                    ])
        dlg_fopen.props.mnemonics_visible = True

        filter_patterns = [
                ("Document", [
                    "*.pdf",
                    "*.xps", "*.oxps",
                    "*.svg",
                    "*.cbz",
                    "*.tiff", "*.jfif",
                    "*.jpeg", "*.jpg",
                    "*.png",
                    "*.gif",
                    "*.html",
                    "*.epub",
                    "*.gprf",
                    "*.fb2",
                    "*.zip",
                    ]),
                ("*.PDF", ["*.pdf"]),
                ("*.xps", ["*.xps", ".oxps"]),
                ("*.svg", ["*.svg"]),
                ("*.cbz", ["*.cbz"]),
                ("*.html", ["*.html"]),
                ("*.epub", ["*.epub"]),
                ("*.gprf", ["*.gprf"]),
                ("*.fb2", ["*.fb2"]),
                ("*.zip", ["*.zip"]),
                ("Image", ["*.tiff", "*.jfif",
                    "*.jpeg", "*.jpg",
                    "*.png", "*.gif"]),
                ("All", ["*.*"]),
            ]

        for name, patterns in filter_patterns:
            ff = Gtk.FileFilter()
            ff.set_name(name)
            patterns_u = [x.upper() for x in patterns if x.islower()]
            if len(patterns_u) > 0:
                patterns.extend(patterns_u)
            for p in patterns:
                ff.add_pattern(p)
            dlg_fopen.add_filter(ff)

        def _on_open_response(dlg, res):
            if (res == Gtk.ResponseType.ACCEPT):
                file_list = dlg.get_files()
                self.application.open_files(file_list)
            dlg.destroy()
        dlg_fopen.connect("response", _on_open_response)
        dlg_fopen.show_all()
        dlg_fopen.present()
        self.application.gapp.add_window(dlg_fopen)

    def open_path(self, action, param):
        """Open document using file path"""
        if not param:
            return
        path = param.get_string()

        app = self.application
        dw = DocumentWindow(app, path)
        app.gapp.add_window(dw.window_main)
        app.actions.load_win_actions(dw.window_main)

class Application:
    def __init__(self, args):
        self.db = None
        self.application_control = None
        self.setup_app(args)

    def setup_app(self, args):
        appid = const.APPID
        if args.new_instance:
            pid = os.getpid()
            appid = f"{appid}.pid_{pid}"
        log.debug(f"APPID: {appid}")

        self.gapp = Gtk.Application(application_id=appid,
                flags=Gio.ApplicationFlags.HANDLES_OPEN)
        self.gapp.application = self
        self.gapp.connect("startup", self.on_app_startup)
        self.gapp.connect("activate", self.on_app_activate)
        self.gapp.connect("open", self.on_open)
        self.gapp.connect("shutdown", self.on_shutdown)
        self.gapp.register()

    def on_app_startup(self, gapp):
        self.application_control = ApplicationControl(self.gapp)

        get_workspace_size()
        self.load_db()

        from . import actions
        self.actions = actions.Actions(self)

        menu_path = os.path.join(const.PKG_DATA_DIR, const.MENU_FILE)
        menu_xml = Gtk.Builder.new_from_file(menu_path)

        self.win_menu = menu_xml.get_object("win-menu")
        app_menu = menu_xml.get_object("app-menu")
        self.gapp.set_app_menu(app_menu)

        def _load_rest():
            """delay loading the rest to speed up startup time"""
            #self.load_actions()
            if self.db:
                config = self.db.load_config()
                enable_im_matcher = config.get("enable_im_matcher", False)
                if enable_im_matcher:
                    im_json = config.get("im_matcher_data", None)
                    from .treematcher import tree_matcher
                    tree_matcher.load_im_matcher(im_json)

        GLib.idle_add(_load_rest)

    @property
    def document_windows(self):
        """Return dict of current {path: DocumentWindow} """
        win_list = self.gapp.get_windows()
        doc_win_list = [x.document_window for x in win_list
                if hasattr(x, "document_window")]
        doc_wins = {x.doc_view.path: x for x in doc_win_list}
        return doc_wins

    def on_open(self, gapp, gfile_list, file_count, hint):
        self.open_files(gfile_list)

    def open_files(self, gfile_list):
        doc_wins = self.document_windows

        for gfile in gfile_list:
            path = gfile.get_path()
            if path in doc_wins:
                doc_wins[path].present()
                continue

            self.application_control.do_action("open_path", path)
            #dw = DocumentWindow(self, path)
            #self.gapp.add_window(dw.window_main)
            #self.actions.load_win_actions(dw.window_main)

    def load_db(self):
        if not self.db:
            from . import docdb
            self.db = docdb.DocumentDB(const.USER_DATABASE)
        config = self.db.load_config()
        #for k,v in config.items():log.debug("{}: {} {}".format(k,type(v),v))

    def on_app_activate(self, gapp):
        self.application_control.do_action("open_file_dialog")

    def on_shutdown(self, gapp):
        log.debug("Shutting down")
        if self.db:
            self.db.close()
            self.db = None

    def run(self, argv):
        self.gapp.run(argv)

    def quit(self):
        wins = self.document_windows
        for k in wins:
            wins[k].document_control.do_action("close_doc")

        self.gapp.quit()

def get_workspace_size():
    """Hack to get desktop size"""
    global WORKSPACE_WIDTH, WORKSPACE_HEIGHT
    if WORKSPACE_WIDTH > 0 or WORKSPACE_HEIGHT > 0:
        return

    portion = .90
    display = Gdk.Display.get_default()
    monitor = display.get_primary_monitor()
    size = monitor.props.workarea
    WORKSPACE_WIDTH = int(size.width * portion)
    WORKSPACE_HEIGHT = int(size.height * portion)
    log.debug(f"Workspace: {WORKSPACE_WIDTH} x {WORKSPACE_HEIGHT}")
    return
    import cairo
    def _on_size_alloc(win, *args):
        if not win.is_maximized():
            return
        size = win.get_size()
        WORKSPACE_WIDTH = size.width
        WORKSPACE_HEIGHT = size.height
        log.debug("desktop size: {}".format(size))
        win.disconnect(win.connection_id)
        win.destroy()
        Gtk.main_quit()

    # Create the window, connect the signal, then maximize it
    w = Gtk.Window()
    w.connection_id = w.connect('size-allocate', _on_size_alloc)
    w.props.opacity = 0
    w.props.decorated = False
    w.props.accept_focus = False
    w.props.focus_on_map = False
    w.props.skip_pager_hint = True
    w.props.skip_taskbar_hint = True
    w.move(0, 0)
    # create a shape window, hopely invisible
    w.shape_combine_region(cairo.Region(cairo.RectangleInt(0, 0, 100, 100)))
    w.set_default_size(10, 10)
    w.resize(10, 10)
    w.maximize()
    w.show_all()
    Gtk.main()

def setup_arg_parser(appname=None):
    if not appname:
        appname = os.path.basename(sys.argv[0])
        if appname.endswith(".py"):
            appname = appname[:len(appname) - 3]

    import argparse
    parser = argparse.ArgumentParser(
        description=f"{appname} {const.__version__} Ebook Reader",
        add_help=False)
    parser.add_argument("--version", action="version",
            version=const.__version_id__)

    parser.add_argument("-n", "--new-instance", action="store_true",
            help="Open new instance, not a new window in running instance.")

    parser.add_argument('-h', '--help', action='store_true',
            help='show this help message and exit')

    parser.add_argument("-D", "--debug", action="store_true",
            help="debug run")

    return parser

def setup_log(log_level=None):
    global log
    rlog = logging.getLogger()
    if __name__ == "__main__" and not rlog.hasHandlers():
        # setup root logger
        ch = logging.StreamHandler()
        fmt_str = "%(levelname)s:%(name)s:: %(message)s"
        if log_level == DEBUG:
            fmt_str = "%(levelname)s:%(funcName)s()[%(lineno)s]:: %(message)s"
        formatter = logging.Formatter(fmt_str)
        ch.setFormatter(formatter)
        rlog.addHandler(ch)

    log = logging.getLogger(__name__)

    if log_level is not None:
        log.setLevel(log_level)
        rlog.setLevel(log_level)

def main():
    global DEBUG
    parser = setup_arg_parser(const.APPNAME)
    args, argv_ui = parser.parse_known_args()
    #print(args, argv_ui); return
    if args.help:
        parser.print_help()
        argv_ui.insert(0, "--help")
    argv_ui.insert(0, sys.argv[0])
    DEBUG = args.debug

    def set_stdio_encoding(enc=NATIVE):
        import codecs; stdio = ["stdin", "stdout", "stderr"]
        for x in stdio:
            obj = getattr(sys, x)
            if not obj.encoding: setattr(sys, x, codecs.getwriter(enc)(obj))
    set_stdio_encoding()

    log_level = logging.INFO
    if DEBUG:
        log_level = logging.DEBUG
    setup_log(log_level)

    from . import util
    app = Application(args)
    util.InitSignal(app.quit)
    #get_workspace_size()
    app.run(argv_ui)

if __name__ == '__main__':
    main()

