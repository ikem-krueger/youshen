#!/usr/bin/python3
# vim:fileencoding=utf-8:sw=4:et
"""
"""
from __future__ import print_function, unicode_literals, absolute_import, division
import sys
import os
import io
import logging as log

NATIVE=sys.getfilesystemencoding()

from gi.repository import GObject, GLib, Gtk, Gdk, Gio
from . import util

class ActionControlBase:
    """Base Class to handle GAction callbacks"""
    def __init__(self, action_map):
        self.action_map = action_map

    def get_action(self, action_name):
        action = self.action_map.lookup_action(action_name)
        return action

    def do_action(self, action_name, *args):
        action = self.get_action(action_name)
        if action is None:
            return

        args_new = []
        vtype = action.get_parameter_type()
        if len(args) > 0:
            for arg in args:
                args_new.append(util.marshall_variant(vtype, arg))

        action.activate(*args_new)

    def change_action_state(self, action_name, value):
        action = self.get_action(action_name)
        vtype = action.get_state_type()
        value = util.marshall_variant(vtype, value)
        if action is not None:
            action.change_state(value)

def add_action_entries(gaction_map, entries, *user_data):
    """Add action entries to GActionMap,
    GActionMap's are Gtk.Application, Gtk.ApplicationWindow"""
    def _process_action(name, activate=None, parameter_type=None,
            state=None, change_state=None):
        if state is None:
            action = Gio.SimpleAction.new(name, parameter_type)
        else:
            state = util.marshall_variant(parameter_type, state)
            action = Gio.SimpleAction.new_stateful(name,
                    parameter_type, state)
        if activate is not None:
            action.connect('activate', activate, *user_data)
        if change_state is not None:
            action.connect('change-state', change_state, *user_data)
        gaction_map.add_action(action)
    for e in entries:
        _process_action(*e)

def add_property_action_entries(gaction_map, entries):
    """Add PropertyAction entries to GActionMap,
    GActionMap's are Gtk.Application, Gtk.ApplicationWindow"""
    def _process_action(name, obj, prop_name, invert_boolean=False):
        action = Gio.PropertyAction.new(name, obj, prop_name)
        # FIXME: invert_boolean is construct only!!
        #action.props.invert_boolean = invert_boolean
        gaction_map.add_action(action)
    for e in entries:
        _process_action(*e)

def group_accels(accels):
    """Group accelerator keys into single or with_mods"""
    singles = []
    with_mods = []
    for accel in accels:
        res = Gtk.accelerator_parse(accel)
        if res.accelerator_mods == 0:
            singles.append(accel)
        else:
            with_mods.append(accel)
    return singles, with_mods

EDIT_ACCELS = ["<Ctrl>v", "<Ctrl>c", "<Ctrl>x", "<Ctrl>Insert"]

class Actions:
    def __init__(self, app):
        self.accel_maps = []
        self.app = app
        self.gapp = app.gapp
        self.load_app_actions()
        self.load_accels()

    def load_win_actions(self, win):
        doc_win = win.document_window
        dcontrol = doc_win.document_control
        doc_view = doc_win.doc_view

        # action name cannot have "-" in it.
        simple_action_names = [
                "page_down", "page_up", "step_down", "step_up",
                "page_left", "page_right",
                "doc_or_tree_view_page_down", "doc_or_tree_view_page_up",
                "doc_or_tree_view_step_down", "doc_or_tree_view_step_up",
                "doc_or_tree_view_left", "doc_or_tree_view_right",
                "go_home", "go_end",

                "zoom_in", "zoom_out", "zoom_in_more", "zoom_out_more",
                "zoom_origin", "zoom_to_width", "zoom_to_page",

                "rotate_clockwise",
                "toggle_skip_draw_background",
                "crop_page",
                "selected_to_clipboard",
                "save_page_as",

                "go_backward", "go_forward", "clear_history",

                "show_properties",
                "close_doc",
                ]
        action_entries = [[x, getattr(dcontrol, x)]
                for x in simple_action_names]
        action_entries.extend([
                ["toggle_invert_color", None, None,
                    GLib.Variant.new_boolean(doc_view.invert_color),
                    dcontrol.toggle_invert_color],
                ["toggle_invert_background", None, None,
                    GLib.Variant.new_boolean(doc_view.invert_background),
                    dcontrol.toggle_invert_background],
                ["toggle_scroll_mark", None, None,
                    GLib.Variant.new_boolean(doc_view.indicate_last_read),
                    dcontrol.toggle_scroll_mark],
                ["toggle_scroll_mark_draw_line", None, None,
                    GLib.Variant.new_boolean(
                        doc_view.indicate_last_read_draw_line),
                    dcontrol.toggle_scroll_mark_draw_line],
                ["set_num_columns", None, GLib.VariantType("s"),
                    str(doc_view.num_columns), dcontrol.set_num_columns],

                ["toggle_show_sidebar", None, None,
                    GLib.Variant.new_boolean(doc_win.show_sidebar),
                    dcontrol.toggle_show_sidebar],
            ])

        add_action_entries(win, action_entries)

        property_action_entries = [
                ]
        add_property_action_entries(win, property_action_entries)

    def remove_single_key_maps(self, win, extra=[]):
        # remove single key bindings
        actions = self.gapp.list_action_descriptions()

        removed_key_maps = []
        #for act, accels in self.accel_maps:
        for act in actions:
            accels = self.gapp.get_accels_for_action(act)
            singles, with_mods = group_accels(accels)
            if len(singles) > 0:
                self.gapp.set_accels_for_action(act, with_mods)
                removed_key_maps.append([act, accels])
        self.removed_key_maps = removed_key_maps

    def reset_single_key_maps(self, win):
        # add back removed single key bindings
        for act, accels in self.removed_key_maps:
            self.gapp.set_accels_for_action(act, accels)
        self.removed_key_maps = []

    def update_key_maps(self, accel_maps):
        for act, accels in accel_maps:
            self.gapp.set_accels_for_action(act, accels)

    def load_app_actions(self):
        """Load gio actions into gtk application"""
        # entry: name, activate, parameter_type, state, change_state
        acontrol = self.app.application_control
        simple_action_names = [
                "open_file_dialog",
                "quit",
                ]
        action_entries = [[x, getattr(acontrol, x)]
                for x in simple_action_names]
        action_entries.extend([
                ["open_path", acontrol.open_path, GLib.VariantType("s")],
            ])
        add_action_entries(self.gapp, action_entries)

        property_action_entries = [
                ]
        add_property_action_entries(self.gapp, property_action_entries)

    def load_accels(self, accel_maps=None):
        if not accel_maps:
            accel_maps = [
                    ["app.open_file_dialog", ["<Control>o"]],
                    ["win.show_properties",["<Alt>Return"]],
                    ["win.close_doc", ["<Control>w"]],
                    ["app.quit", ["<Control>q"]],

                    ["win.selected_to_clipboard", ["<Control>c"]],
                    ["win.rotate_clockwise", ["<Control>Right"]],

                    ["win.toggle_show_sidebar", ["F9"]],
                    ["win.zoom_in", ["<Control>plus"]],
                    ["win.zoom_out", ["<Control>minus"]],
                    ["win.zoom_origin", ["<Control>0"]],
                    ["win.zoom_to_width", ["w"]],
                    ["win.zoom_to_page", ["p"]],

                    ["win.page_down", ["space"]],
                    ["win.page_up", ["b"]],
                    ["win.step_down", ["j"]],
                    ["win.step_up", ["k"]],
                    ["win.page_left", ["h"]],
                    ["win.page_right", ["l"]],

                    ["win.doc_or_tree_view_page_down", ["Page_Down"]],
                    ["win.doc_or_tree_view_page_up", ["Page_Up"]],
                    ["win.doc_or_tree_view_step_down", ["Down"]],
                    ["win.doc_or_tree_view_step_up", ["Up"]],

                    ["win.doc_or_tree_view_left", ["Left"]],
                    ["win.doc_or_tree_view_right", ["Right"]],

                    ["win.go_home", ["Home"]],
                    ["win.go_end", ["End"]],

                    ["win.zoom_in_more", ["plus", "<Shift>KP_Add"]],
                    ["win.zoom_out_more", ["underscore", "<Shift>KP_Subtract"]],

                    ["win.set_num_columns('1')", ["<Control>1"]],
                    ["win.set_num_columns('2')", ["<Control>2"]],
                    ["win.set_num_columns('3')", ["<Control>3"]],

                    ["win.go_forward", ["<Control>i"]],
                    ["win.go_backward", ["BackSpace"]],

                    ["win.toggle_skip_draw_background", ["<Shift>b"]],
                    ["win.crop_page", ["c"]],
                ]


        accel_index = {x[0]:i for i, x in enumerate(self.accel_maps)}
        for item in accel_maps:
            if item[0] in accel_index:
                self.accel_maps[accel_index[item[0]]] = item
            else:
                self.accel_maps.append(item)
        self.update_key_maps(accel_maps)

def main():
    def set_stdio_encoding(enc=NATIVE):
        import codecs; stdio = ["stdin", "stdout", "stderr"]
        for x in stdio:
            obj = getattr(sys, x)
            if not obj.encoding: setattr(sys,  x, codecs.getwriter(enc)(obj))
    set_stdio_encoding()

    log_level = log.INFO
    log.basicConfig(format="%(levelname)s>> %(message)s", level=log_level)

if __name__ == '__main__':
    main()

